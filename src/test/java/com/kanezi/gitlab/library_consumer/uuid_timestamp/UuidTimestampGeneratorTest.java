package com.kanezi.gitlab.library_consumer.uuid_timestamp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UuidTimestampGeneratorTest {

    @Test
    void generatesUniqueValues() {
        String firstValue = UuidTimestampGenerator.generate();
        String secondValue = UuidTimestampGenerator.generate();

        assertNotEquals(firstValue, secondValue);

    }

}